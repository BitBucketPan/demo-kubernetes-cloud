package com.example.communication.controller;

import com.example.communication.property.UrlProperty;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CommunicationController.class)
public class CommunicationControllerTest {

    @Autowired
    UrlProperty urlProperty;

    @Autowired
    MockMvc mockMvc;

    @MockBean
    RestTemplate restTemplate;

    @Test
    public void request() throws Exception {
        final String restMessage = "Hello!";

        when(restTemplate.getForObject(urlProperty.getService(), String.class)).thenReturn(restMessage);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/request"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.holder.url").value(urlProperty.getService()))
                .andExpect(jsonPath("$.restMessage").value(restMessage))
                .andReturn();

        Assert.assertEquals("application/json", mvcResult.getResponse().getContentType());
    }
}
