package com.example.communication.response;

import lombok.Data;

@Data
public class MessageResp {

    private String restMessage;
    private StringHolder holder;

    @Data
    public static class StringHolder {
        private String url;
    }
}
