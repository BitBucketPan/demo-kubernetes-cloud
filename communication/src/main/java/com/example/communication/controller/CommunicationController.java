package com.example.communication.controller;

import com.example.communication.property.UrlProperty;
import com.example.communication.response.MessageResp;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/request")
@RequiredArgsConstructor
public class CommunicationController {

    private final UrlProperty urlProperty;

    private final RestTemplate restTemplate;

    @GetMapping
    public MessageResp request() {
        String message = restTemplate.getForObject(urlProperty.getService(), String.class);

        MessageResp messageResp = new MessageResp();
        MessageResp.StringHolder stringHolder = new MessageResp.StringHolder();
        stringHolder.setUrl(urlProperty.getService());
        messageResp.setHolder(stringHolder);
        messageResp.setRestMessage(message);

        return messageResp;
    }
}
