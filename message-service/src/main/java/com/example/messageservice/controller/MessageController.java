package com.example.messageservice.controller;

import com.example.messageservice.properties.MessageProperty;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/message")
@RequiredArgsConstructor
public class MessageController {

    private final MessageProperty messageProperty;

    @GetMapping
    public String message() {
        return messageProperty.getSource();
    }

}
