package com.example.messageservice.controller;

import com.example.messageservice.properties.MessageProperty;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MessageController.class)
public class MessageControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    MessageProperty messageProperty;

    @Test
    public void message_returnString() throws Exception {
        Assert.assertNotNull(messageProperty);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/message"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Assert.assertEquals("text/plain;charset=UTF-8", mvcResult.getResponse().getContentType());
        Assert.assertEquals(messageProperty.getSource(), mvcResult.getResponse().getContentAsString());
    }
}
